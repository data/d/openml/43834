# OpenML dataset: Historical-Financials-Data-for-3000-stocks

https://www.openml.org/d/43834

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
Getting access to high-quality historical stock market data can be very expensive and/or complicated; parsing SEC 10-Q filings direct from the SEC EDGAR is difficult due to the varying structures of filings and SEC filing data from providers such as Quandl charge hundreds or thousands of dollars in yearly fees to get access to them. Here, I provide an easy-to-use, straight from the source database of parsed financials information from SEC 10-Q filings for more than 3000 stocks.
Content
The quarterly financials are provided in a single .csv file, quarterly_financials.csv
50 of the data is NaN either because the field wasn't detected by my XBRL parsing system or the field wasn't addressed in the SEC filing.
Acknowledgements
All the data is scraped from the SEC from the XBRL files.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43834) of an [OpenML dataset](https://www.openml.org/d/43834). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43834/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43834/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43834/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

